// --- false if final bundle will be served from a web server, true if it will be served using
// file:/// (or qrc:///)
const RELATIVE_PATH = false

const path = require('path');
const webpack = require('webpack');

// Remove this line once the following warning goes away (it was meant for webpack loader authors not users):
// 'DeprecationWarning: loaderUtils.parseQuery() received a non-string value which can be problematic,
// see https://github.com/webpack/loader-utils/issues/56 parseQuery() will be replaced with getOptions()
// in the next major version of loader-utils.'
process.noDeprecation = true;

module.exports = (options) => ({

  // --- helps against 'fs' error during npm run build:dll.
  node: {
    fs: 'empty',
  },

  entry: options.entry,
  output: Object.assign({
    // --- output: js/build.js
    path: path.resolve(process.cwd(), 'build'),
    publicPath: RELATIVE_PATH ? './' : '/',
  }, options.output),
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              [
                "env",
                {
                  "modules": false
                }
              ],
              "react",
              "stage-0"
            ],
            plugins: [
              'operator-overload',
              [
                "transform-object-rest-spread",
                {
                  "useBuiltIns": false,
                },
              ],
              // --- necessary??
              [
                "transform-runtime",
                {
                  "polyfill": false,
                  "regenerator": true
                }
              ],
            ]
          }
        },
      },

      /*
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "eslint-loader",
        options: {
          // supports newer features than eslint.
          parser: 'babel-eslint',
        },
      },
      */

      {
        test: /\.css$/,
        exclude: /node_modules/,
        use: ['style-loader', 'css-loader'],
      },
      {
        // --- preprocess 3rd party .css files located in node_modules
        test: /\.css$/,
        include: /node_modules/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.shader$/,
        use: 'file-loader',
      },
      {
        test: /\.(eot|svg|otf|ttf|woff|woff2)$/,
        use: 'file-loader',
      },
      {
        test: /\.(jpg|png|gif|ogg)$/,
        use: [
          'file-loader',
          {
            loader: 'image-webpack-loader',
            options: {
              progressive: true,
              optimizationLevel: 7,
              interlaced: false,
              pngquant: {
                quality: '65-90',
                speed: 4,
              },
            },
          },
        ],
      },
      {
        test: /\.html$/,
        use: 'html-loader',
      },
      {
        test: /\.json$/,
        use: 'json-loader',
      },
      {
        test: /\.(mp4|webm)$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 10000,
          },
        },
      },
    ],
  },
  plugins: options.plugins.concat([
    new webpack.ProvidePlugin({
      fetch: 'exports-loader?self.fetch!whatwg-fetch',
    }),

    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV),
        FORCE_STATIC_MAP: JSON.stringify(process.env.FORCE_STATIC_MAP),
      },
    }),
    new webpack.NamedModulesPlugin(),
  ]),
  resolve: {
    modules: ['app', 'node_modules'],
    extensions: [
      '.js',
      '.jsx',
      '.react.js',
    ],
    mainFields: [
      'browser',
      'jsnext:main',
      'main',
    ],
  },
  devtool: options.devtool,
  // --- make web variables accessible to webpack, e.g. window
  target: 'web',
  performance: options.performance || {},
});
