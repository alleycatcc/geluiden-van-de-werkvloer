defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
} from 'stick-js'

import {
  isNotEmptyObject,
  subtract,
  logAndroidPerf,
} from '../../utils/utils.js'

import {
  toJS, fromJS, getIn,
  listToOrderedSet,
  add as iAdd, del as iDelete,
  set,
} from '../../utils/utils-immutable'

import {
  FETCH_FORECAST,
  FETCH_FORECAST_SUCCESS,
  FETCH_FORECAST_ERROR,
  ROUTE_CHANGED,
  CHANGE_LOCATION,
  UPDATE_DEBUG,
  UPDATE_SHOW_RAIN,
} from '../../containers/App/constants'

import {
  // makeMakeSelectHasName,
} from './selectors'

const initialState = fromJS ({
  debug: false,

  // --- `true` means the reducer is totally corrupted and the app should halt.
  error: false,

  showRain: false,
})

export default (state = initialState, action) => {
  const { type, data, } = action
  switch (type) {
    case UPDATE_DEBUG:
      console.log ('UPDATE_DEBUG in reducer', data)
      return state
        .set ('debug', data)
    case UPDATE_SHOW_RAIN:
      return state
        .set ('showRain', data)
    default:
      return state
  }
}
