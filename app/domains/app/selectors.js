defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  dot, dot1, whenOk, ifOk,
} from 'stick-js'

import { createSelector, defaultMemoize as memoize, } from 'reselect'

import {
  length, isNotEmptyObject, notOk,
  logWith,
} from '../../utils/utils.js'

import {
  createLengthEqualSelector,
} from '../../utils/utils-select'

import {
  find as iFind,
  filter as iFilter,
  sortBy as iSortBy,
  get, getIn, size, toJS, fromJS,
  valueSeq,
} from '../../utils/utils-immutable'

import {
  configGetOr, configGet,
  configGetComponent, configGetsComponent,
} from '../../config'

const selectApp = get ('app')

export const makeSelectDebug = _ => createSelector (
  selectApp,
  get ('debug'),
)

export const makeSelectShowRain = _ => createSelector (
  selectApp,
  get ('showRain'),
)
