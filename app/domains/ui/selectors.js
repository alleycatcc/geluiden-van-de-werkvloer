defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

const TEST_EVENTS_NUM_ERRORS = 0

import { createSelector, } from 'reselect'

import {
  pipe, compose, composeRight,
  add, whenOk,
} from 'stick-js'

import {
  length,
} from '../../utils/utils.js'

import {
  get, toJS,
  update,
  sortBy as iSortBy,
  toOrderedMap,
} from '../../utils/utils-immutable'

const selectDomain = get ('ui')

// export const makeSelectAllowDelete = _ => createSelector (
//   selectDomain,
//   get ('allowDelete'),
// )
// export const makeSelectUploadedEventsResults = _ => createSelector (
//   selectDomain,
//   get ('uploadedEventsResults') >> whenOk (
// 	update ('numError', TEST_EVENTS_NUM_ERRORS | add) // >> toJS
//   ),
// )
// export const makeSelectUploadedEventsResultsLastSuccess = _ => createSelector (
//   makeSelectUploadedEventsResults (),
//   whenOk (get ('lastSuccess') >> toOrderedMap >> iSortBy (
//     (_, name) => name,
//   ))
// )
