defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  curry,
} from 'ramda'

import {
  pipe, compose, composeRight,
  id, tap, ok, whenOk, dot, dot1, notOk, isTrue,
  guard, otherwise, condS, guardV,
} from 'stick-js'

import React, { PureComponent, Component, } from 'react'

import Alert from 'react-s-alert'

import {
  ierror,
} from './utils.js'

const msg = condS ([
  notOk     | guardV (''),
  isTrue    | guardV ('Oops, something went wrong!'),
  otherwise | guard (id),
])

const _error = opts => txt => Alert.error (txt, opts)

export const warning = msg >> Alert.warning
export const error = msg >> _error ({})
export const info = msg >> Alert.info
export const success = msg >> Alert.success
export const close = id => Alert.close (id)
export const closeAll = Alert.closeAll

// need a better name for this.
// it's for showing a specific programmer error in the console and a generic 'oops' in the bubble.
export const ierrorError = ierror >> tap (_ => true | error)

export const error1 = curry ((opts, x) => x | msg | _error (opts))
