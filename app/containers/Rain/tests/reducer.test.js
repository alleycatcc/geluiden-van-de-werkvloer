
import { fromJS } from 'immutable';
import rainReducer from '../reducer';

describe('rainReducer', () => {
  it('returns the initial state', () => {
    expect(rainReducer(undefined, {})).toEqual(fromJS({}));
  });
});
