const DO_RAIN = false

defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  mergeM,
  die,
  appendTo, tap,
  whenTrue,
} from 'stick-js'

import React, { PureComponent, Component, } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { connect, } from 'react-redux'
import { FormattedMessage, } from 'react-intl'
import { createStructuredSelector, } from 'reselect'

import injectSaga from 'utils/injectSaga'
import injectReducer from 'utils/injectReducer'
import makeSelectRain from './selectors'
import reducer from './reducer'
import saga from './saga'
import messages from './messages'

import {
  mapX, tellIf,
  iwarn, ierror,
  logWith,
} from '../../utils/utils.js'

import {
  toJS,
} from '../../utils/utils-immutable'

import {
  whyYouRerender,
} from '../../utils/utils-react.js'

import {
  error as alertError,
} from '../../utils/utils-alert.js'

import {
  configGetOr, configGet,
} from '../../config'

import { go as rainGo, } from '../../../rain-webgl-stick/src/index'

const canvasStyle = {
  top: '0px',
  left: '0px',
  position: 'absolute',
}

// --- not sure why, but hardcoding height and width as attributes of canvas works right, but as
// style properties (whether as percentage or literals) results in the drops looking ugly.

class Canvas extends React.PureComponent {
  constructor (props) {
    super (props)
    this.ref = void 8

    // --- very unreact, but we get the height and width from the window.
    const { innerWidth, innerHeight, } = window

    this | mergeM ({
      ref: void 8,
      width: innerWidth,
      height: innerHeight,
    })
  }

  render () {
    const { width, height, } = this
    return <canvas width={width} height={height} style={canvasStyle}
      ref={ref => this.ref = ref}
    />
  }
}

// import { } from '../App/actions'
// import { } from '../../domains/api/selectors'
// import { } from '../../domains/domain/selectors'
// import { } from '../../domains/ui/selectors'
// import { } from '../../components/xxx'

const assignTo = o => p => v => o [p] = v
const configGetOrDie = p => configGet (p) || die ('bad config')

const getWeatherConfig = String >> appendTo (['raindrops', 'weather']) >> configGetOrDie

export class Rain extends React.PureComponent {
  constructor (props) {
    super (props)
    const instanceVars = {
      canvas: void 8,
    }
    this | mergeM (instanceVars)
  }

  componentDidUpdate () {
    const { props, canvas, } = this
    const { active, amount = 2, } = props
    const weatherConfig = amount | getWeatherConfig

    const images = ['raindrops', 'images'] | configGetOrDie

    if (! active) return

    const { ref, } = canvas
    rainGo (ref, { images, weatherConfig })
  }

  render () {
    const { active, } = this.props
    return active ? <Canvas
      ref={assignTo (this) ('canvas')}
    /> : null
  }
}

Rain.propTypes = {
}

const mapStateToProps = createStructuredSelector ({
})

const mapDispatchToProps = (dispatch) => ({
})

export default Rain
  | connect       (mapStateToProps, mapDispatchToProps)
  | injectSaga    ({ saga, key: 'rain' })
  // --- local reducer (optional)
  | injectReducer ({ reducer, key: 'rain' })

