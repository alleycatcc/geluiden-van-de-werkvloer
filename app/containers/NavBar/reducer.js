defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  ok, mergeFrom, whenOk, dot, xMatch,
  whenPredicate, eq,
} from 'stick-js'

import {
  toJS, fromJS,
  updateIn,
} from '../../utils/utils-immutable'

// import { } from '../../containers/App/constants'

const initialState = fromJS ({
})

export default (state = initialState, action) => {
  switch (action.type) {
    default:
      return state
  }
}


