// --- if the start time is 2pm, then the clock goes all the way around to 1:59 am, weird but true
// --- clock is 720
// --- have to keep track of begin angle, then everything is rotated, e.g. to detect angle
// 12 / 0 is fubar

defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  divide, multiply,
  min as rMin, max,
  add, subtract,
  not,
  and, or,
  head, last,
  clamp,
  allPass,
  all,
  keys,
  always,
} from 'ramda'

import {
  pipe, compose, composeRight,
  map, split, id, modulo,
  sprintfN, xReplace, ifNo, whenOk, ifOk, ifYes, blush, whenYes,
  sprintf1,
  condS, guard, otherwise, xMatch, appendTo,
  dot, ifPredicate, eq,
  ok, whenTrue,
  assocM, mergeM,
  lets, letS,
  list, gte, lt, gt,
  neu1,
  dot2,
  divideBy, minus,
  subtractFrom,
  plus,
  factory,
  isInteger,
  bind, invoke,
  prop,
} from 'stick-js'

import {
  mapX, tellIf,
  iwarn, ierror,
  logAndroid,
  logAndroidPerf,
  logWith,
  memoize,
} from '../../utils/utils.js'

import {
  whyYouRerender,
} from '../../utils/utils-react.js'

import {
  error as alertError,
} from '../../utils/utils-alert.js'

import {
  configGetOr, configGet,
  configGetComponent, configGetsComponent,
} from '../../config'

import React, { PureComponent, Component, } from 'react'
import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import {
  makeSelectForecastByTime,
} from '../../domains/domain/selectors'

import stimeMod from '../../stime'
import { circleMonoid, force as cmForce, } from '../../circleMonoid'

const pi = Math.PI
const { min, } = Math

const deg2rad = memoize (divideBy (180) >> multiply (pi))
const rad2deg = memoize (divideBy (pi)  >> multiply (180))

const twopi  = 2 * pi
const fourpi = 4 * pi

const normaliseAngle = (max) => (x => (null, x | condS ([
  max  | gte | guard (x => x | minus (max) | normaliseAngle (max)),
  0    | lt  | guard (x => x | add   (max) | normaliseAngle (max)),
  otherwise  | guard (id),
])))
  >> Math.floor

const normaliseAngleDeg    = normaliseAngle (360)
const normaliseAngleRad    = normaliseAngle (fourpi)

const makeArc = (_ => {
  // --- figure out if the arc is 'large', in the svg arc sense.
  const isLarge = (startAngle, endAngle) =>
    (startAngle - endAngle) | condS ([
      0 | gte | guard (diff => startAngle < 90
        ? false      // --- both before 3:00
        : diff > 180 // --- both after 3:00
      ),
      // --- start before 3:00, end after 3:00
      otherwise | guard (-180 | gt),
    ])

  const coords = memoize ((sa, ea) => lets (
    _ => cx + radius * Math.cos (sa),
    _ => cy - radius * Math.sin (sa),
    _ => cx + radius * Math.cos (ea),
    _ => cy - radius * Math.sin (ea),
    (x1, y1, x2, y2) => [x1, y1, x2, y2],
  ))

  return (cx, cy, radius, startAngle, endAngle) => lets (
    _ => startAngle | deg2rad,
    _ => endAngle   | deg2rad,
    _ => isLarge (startAngle, endAngle) | Number,
    (sa, ea) => coords (sa, ea),
    (_1, _2, large, [x1, y1, x2, y2]) =>
      [x1, y1, radius, radius, 0, large, 1, x2, y2]
      | sprintfN ('M%s %s A %s %s %s %s %s %s %s')
  )
}) ()

const [rad90, rad180, rad270, rad360] = [
  pi / 2,
  pi,
  pi / 2 * 3,
  pi * 2,
]

const timeStrToMinutes = memoize ((str) => lets (
  _ => str | split (':') | map (Number),
  ([hh, mm]) => (hh % 12) * 60 + mm,
))

const dateToMinutesX = (geth, getm) => letS ([
  dot (geth),
  dot (getm),
  (_, h, m) => h * 60 + m,
])

const dateToMinutesTZ   = dateToMinutesX ('getHours', 'getMinutes')
const dateToMinutesUTC  = dateToMinutesX ('getUTCHours', 'getUTCMinutes')

// --- ok to memoize on Date? xxx

// --- [90, 450), i.e. starting at 3:00, counter-clockwise.
const dateToAngleCanonical = memoize (
  dateToMinutesTZ
  >> modulo (720)
  >> ((mins) => 90 - mins * 360 / 720)
  >> normaliseAngleDeg
)

// --- [0, 720)
const _dateToAngle = memoize (
  dateToMinutesTZ
  >> ((mins) => mins * 360 / 720)
  >> Math.floor
)

const dateToAngle360 = memoize (
  _dateToAngle >> modulo (360)
)

const dateToAngle720 = memoize (
  _dateToAngle >> modulo (720)
)

const [
    cx, cy, radius, segmentLength, armLength,
    armStrokeWidth, armStrokeColor, armFillColor,
    svgPadding,
    borderActiveStrokeWidth, borderInactiveStrokeWidth, borderInactiveStrokeColor,
    timeColor,
  ] = [
    'cx', 'cy', 'radius', 'segmentLength',
    ['arm', 'length'], ['arm', 'stroke', 'width'],
    ['arm', 'stroke', 'color'],
    ['arm', 'fill', 'color'],
    ['padding'],
    ['borderActive', 'stroke', 'width'],
    ['borderInactive', 'stroke', 'width'],
    ['borderInactive', 'stroke', 'color'],
    'timeColor',
  ]
  | configGetsComponent ('Clock')

// --- do not set the height or width of svg, because otherwise the mouse coordinates are difficult
// to transform to model coordinates.
const SvgS = styled.svg`
  padding: ${svgPadding}px;
  width: ${svgPadding * 2 + radius * 2}px;
  height: ${svgPadding * 2 + radius * 2}px;
  display: block;
  margin: auto;
  margin-top: 10%;
  margin-bottom: 10%;
`

import { createStructuredSelector, } from 'reselect'
import { connect, } from 'react-redux'
import { logServer, } from '../../containers/App/actions'

const ClockSegment = ({ startTime, endTime, fill, stroke, strokeWidth, interact, }) => lets (
  _ => startTime | dateToAngleCanonical,
  _ => endTime   | dateToAngleCanonical,
  (startAngle, endAngle) => makeArc (cx, cy, radius, startAngle, endAngle),
  (_1, _2, d) => <path
    fill={fill}
    stroke={stroke}
    strokeWidth={strokeWidth}
    d={d}
  />,
)

const addMinutes = n => Number >> add (n * 60 * 1000) >> neu1 (Date)

const rgb = sprintfN ('#%02x%02x%02x')
const ifZero = ifPredicate (0 | eq)

// --- strength: [0, 1]
const strengthColor = memoize (
  ifZero (
    _ => [0, 255, 0],
    strength => [255, (255 - 255 * strength) | Math.floor, 0],
  )
  >> rgb
)

const TimeS = styled.div`
  font-family: Arial;
  text-align: center;
  width: 100%;
  // margin: auto;
  // width: 90%;
  div {
    display: inline-block;
  }
  div.__time {
    font-size: 35px;
    width: 70px;
    color: ${ timeColor };
  }
  div.__precipitation {
    font-size: 35px;
    margin-left: 30px;
    color: ${ timeColor };
  }
  .__units {
    margin-left: 5px;
    font-size: 20px;
    color: ${ timeColor };
  }
  .__swatch {
    background-color: ${ prop ('strength') >> strengthColor};
    width: 20px;
    height: 20px;
    margin-left: 5px;
  }
`

const ifAllOk = ifPredicate (all (ok))

const Time = ({ isNow, time, precipitation, strength, }) => [time, precipitation] | ifAllOk (_ =>
  <TimeS strength={strength}>
    <div className='__time'>
      {isNow ? 'nu' : time}
    </div>
    {// →
    }
    <div className='__precipitation'>
      {precipitation | sprintf1 ('%.1f')}
      <div className='__units'>mm/uur</div>
    </div>
    <div className='__swatch'/>
  </TimeS>
) (_ => null)

const precipitationForTime = (stime, forecastByTime) => lets (
  _ => stime.getNearestMinutes (segmentLength, forecastByTime),
  ifOk (
    (nearest) => forecastByTime.get (nearest) | prop ('precipitation'),
    _ => iwarn ('no forecast'),
  )
)

export default class Clock extends PureComponent {
  constructor (props) {
    super (props)
    const { segments, } = props

    this.state = {
      stime:          void 8,
      precipitation: void 8,
      strength:      void 8,

      // --- deg.
      armRot:        void 8,
      showArm:       false,
    }

    this.timeBegin = segments | head | prop ('utcdatetime')
    this.timeEnd   = segments | last | prop ('utcdatetime')

    // [0, 720)
    this.angleBoundBegin = this.timeBegin | dateToAngle720
    this.angleBoundEnd   = this.timeEnd   | dateToAngle720

    if (this.angleBoundEnd < this.angleBoundBegin) {
      this.angleBoundBegin -= 360
      this.angleBoundEnd   += 360
    }

    const [abb, abe] = [this.angleBoundBegin, this.angleBoundEnd]

    // --- xxx
    const ttt = 90
    const uuu = ((360 - ttt) / 2) | Math.floor

    // --- the window is a 720 monoid. the starting and end points are arbitrary, so long as the
    // active region is included. here we center it.
    // the 720 does *not* correspond to am and pm.
    // (if we get so far shifted that the upper bound is below the lower bound, we shift them both
    // by 360 to the left)
    // the 720 is so that we can ensure that a window [0, 360) can be represented as an
    // increasing interval [x, x + 360) within [0, 720).
    // so a window going from 270 to 630 would be either 10am or 10pm: need start time to
    // disambiguate.
    // with this in place, we can convert screen coordinates (mouse-click) into clock coordinates:
    // add or subtract multiples of 360 from the screen click to get it into the 720 range.

    const [windowl, windowr, atEdge] = (abb - uuu) | condS ([
      // --- not possible to center the active region in the window because we would fall off the
      // edge to the left.
      // this is the case with region [1:15am, 4:15am) for example.
      // the downside to doing it like this is that the low clamp becomes unintuitive: if you click
      // left of 12:00, it clamps to the upper bound. xxx
      0 | lt    | guard (_ => [0, 360    , true]),

      // --- centered, all the math works out.
      otherwise | guard (l => [l      , 360 + l, false ]),
    ])

    this.window = {
      windowl,
      windowr,
      uuu,

      atEdge,

      // --- the mouse click is first resolved to the range [0, 360), then shifted into the correct
      // window, then clamped if outside the active range.
      theta: abb,

      monoid: circleMonoid (windowl, windowr),

      // --- beginning of window, so different than this.timeBegin
      timeBegin: this.timeBegin | addMinutes (
        atEdge ? 0 : -1 * 2 * uuu
      ),

      curTime () {
        const shift = this.atEdge ? this.theta - abb : this.theta - this.windowl
        return this.timeBegin | addMinutes (shift *2)
      },
    }

    this.timeBeginMins = this.timeBegin | dateToMinutesUTC

    this.state | mergeM (
      this.updateState ()
    )
  }

  clickEnd () {
    const { onClickEnd, } = this.props
    onClickEnd ()
  }

  clickStart () {
    const { onClickStart, } = this.props
    onClickStart ()
  }

  processEvent (e, ...args) {
    const svg = e.target
    const svgBounds = svg.getBoundingClientRect ()
    const x = e.clientX - svgBounds.left
    const y = e.clientY - svgBounds.top
    const dy = cy - y
    const dx = x - cx
    const { angleBoundBegin: abl, angleBoundEnd: abu, } = this

    const theta = Math.atan (dy / dx)
      | rad2deg
      | add (x < cx ? 180 : 0)
      | Math.floor
      | subtractFrom (90) // --- [-90, 270)
      | circleMonoid (0, 360) | prop ('value') // --- [0, 360)
      | this.window.monoid | prop ('value')
      | clamp (abl, abu)

    this.window | assocM ('theta', theta)
    this.setState (this.updateState ())
  }

  touchEvent (evt) {
    // evt.preventDefault ()
    this.processEvent (evt | prop ('touches') | last)
  }

  render () {
    const { state, props, timeBegin, angleBoundBegin, } = this
    const { stime, precipitation, strength, armRot, } = state
    const { segments, } = props

    const isNow = angleBoundBegin == armRot

    return <div>
      <SvgS
        onMouseUp= {(...args) => {
          this.processEvent (...args)
          this.clickEnd (...args)
        }}
        onMouseDown= {(...args) => {
          this.clickStart (...args)
          this.processEvent (...args)
        }}
        onTouchMove= {(...args) => {
          this.touchEvent (...args)
        }}
        onTouchEnd={(...args) => {
          this.clickEnd (...args)
        }}
        onTouchStart={(...args) => {
          this.clickStart (...args)
          this.touchEvent (...args)
        }}
      >
        {segments | mapX (({ utcdatetime, strength }, idx) => (
          <ClockSegment
            key={idx}
            fill='none'
            stroke={strength | strengthColor}
            strokeWidth={borderActiveStrokeWidth}
            startTime={utcdatetime}
            endTime={utcdatetime | addMinutes (segmentLength)}
          />
        ))}
        {(true || this.state.showArm) | whenTrue (
          _ => <path
            stroke={armStrokeColor}
            fill={armFillColor}
            strokeWidth={armStrokeWidth}
            d={[cx, cy, armLength, armLength] | sprintfN (
              'M%s,%s l0,4 l%s,-2 l0,-4 l-%s,-2'
            )}
            transform={[this.state.armRot - 90, cx, cy] | sprintfN ('rotate(%s %s %s)')}
          />
        )}
        <ClockSegment
          fill='none'
          strokeWidth={borderInactiveStrokeWidth}
          stroke={borderInactiveStrokeColor}
          startTime={this.timeEnd | addMinutes (segmentLength)}
          endTime={this.timeBegin}
        />
      </SvgS>
      <Time
        isNow={isNow}
        time={stime.print ()}
        precipitation={precipitation}
        strength={strength}
      />
    </div>
  }
}

Clock.propTypes = {
}
